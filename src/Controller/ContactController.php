<?php declare(strict_types=1);

namespace JonathanMartz\SupportStaff\Controller;

use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route(defaults={"_routeScope"={"store-api"}})
 */
class ContactController extends StorefrontController
{
    /**
     * @Route("/store-api/support-staff/contacts", name="store-api.contacts", methods={"GET"})
     */
    public function contacts()
    {
        return new JsonResponse(
            [
                'contacts' =>
                    [
                        ['firstname' => 'Jonathan',
                            'lastname' => 'Martz',
                            'role' => 'hawego Shopware Entwickler',],
                        ['firstname' => 'Jonathan',
                            'lastname' => 'Martz',
                            'role' => 'hawego Shopware Entwickler',],
                        ['firstname' => 'Jonathan',
                            'lastname' => 'Martz',
                            'role' => 'hawego Shopware Entwickler',],
                        ['firstname' => 'Jonathan',
                            'lastname' => 'Martz',
                            'role' => 'hawego Shopware Entwickler',]

                    ],
                'mail' => 'kundenservice@hawego.de',
                'card' => '/data/hawego.vcf',
                'phone' => '02266 485301-013',
                'wa' => '+49 174 8134392',
                'oh' => ['Mo-Do' => '7:30-17:00', 'Fr' => '7:30-16:00'],
            ]

        );
    }
}